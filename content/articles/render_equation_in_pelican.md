Title: How to render LaTeX formula in Pelican
Date: 2022-06-14 14:28
Modified: 2022-06-14 14:28
Category: math 
Tags: math, latex, pelican
Slug: rendering-latex-pelican
Authors: Samuel ORTION
lang: en
<!-- Summary:  -->

Rendering $\LaTeX$ formulas in Pelican is easy.

Firstly import the pelican plugin in the proper python environment:

```bash
pip install pelican-render-math
```

Add `render_math` to PLUGINS list in your `pelicanconf.py` file:

```python
PLUGINS = ['render_math']
```

Then type formula in your blog post markdown documents:


```tex
$$
\frac{1}{2}
$$
```
$$
\frac{1}{2}
$$

And that's it !

