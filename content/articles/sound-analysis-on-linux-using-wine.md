Title: Analyse Sound on GNU/Linux using Wine
Author: Samuel ORTION
Date: 2021-03-25 10:40
Category: audio
Tags: bat, bird, ultrasound, syrinx, batsound
lang: en
After recording bats, orthoptera or birds, it is often necesserary to see the spectrograms of the sounds, for instance while analysing [Vigie-Chiro Program](http://www.vigienature.fr/fr/chauves-souris) bat records

The software needed to do so are often only for Windows, in the present article, we will learn how to install these softwares (i.e. Kaleidoscope, Syrinx, Batsound 4, 7-zip, Lupas-Rename).

Install Wine
------------

Wine is a software that enable .exe software to run on UNIX systems such as Linux or Mac OS.

### On Debian and derivatives (Ubuntu...)

Enable 32 bit packages (if you haven't already):
```bash
sudo dpkg --add-architecture i386
```

Download and install the repository key:

```bash
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
```

Add the repository to /etc/apt/sources.list or create a wine.list under /etc/apt/sources.list.d/ with the following content:

```textile
deb https://dl.winehq.org/wine-builds/debian/ buster main 
```

Update packages

```bash 
sudo apt update
```    

Install Wine stable

```bash 
sudo apt install --install-recommends winehq-stable
```

### On Fedora, RHEL, and derivatives

Add repository :
```bash
dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/33/winehq.repo
```

Install stable package :
```bash
dnf install winehq-stable
```

Install Kaleidoscope
--------------------

Kaleidoscope is available on both fedora and debian based distros at [wildlife acoustics](https://www.wildlifeacoustics.com/).

Install Syrinx
--------------

As all following softwares, Syrinx is not available for GNU/Linux, we need Wine to execute the `.exe`.
Syrinx-PC is available at [Google Drive](https://drive.google.com/file/d/0B5ZM90wrDzUOM0ZfYlpDR2l1cU0/view). 
You will also need the config files available at [Google Drive](https://drive.google.com/file/d/0B5ZM90wrDzUOQnBhRjNVRFM1Rkk/view).

You have almost two options : 
You can either right click on the `.exe` installer and select `Open with other application` and `Wine Windows Program Loader`, or either run `wine syrinxalphainst.exe` in Terminal.

### Set up app launcher

You have to create a new file `.local/share/applications/syrinx.desktop`:
```textile
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Syrinx
Comment=Acoustic analysis
Exec=/usr/bin/wine /home/<USER>/.wine/drive_c/Program\ Files\ (x86)/syrinx/Syrinx.exe
Icon=/home/<USER>/.wine/drive_c/Program Files (x86)/syrinx/img/vigie-chiro.png
Terminal=false
```
Change `<USER>` by your username. To have the icon, you can download [`vigie-chiro.png`](images/vigie-chiro.png).
To analyse ultrasound, with a 384 kHz sampling frequency, we have to open `Configs_syrinx/exp384.dsp`; and next Load sound file (`Ctrl+L`). To switch to other sound file in same folder, we can use `alt+arrows`. For more tips, you can view [the video of Charlotte ROEMER (Fr)](https://www.youtube.com/watch?v=BPPSw2FSLxs).

Install and Configure Batsound 4
--------------------------------

The procedure is quite similar with Syrinx-PC installation.

### Set up app launcher

Create a new file `.local/share/applications/batsound.desktop`:

```textile
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Batsound
Comment=Acoustic analysis
Exec=/usr/bin/wine /home/<USER>/.wine/drive_c/Program\ Files\ (x86)/Pettersson/Batsound4/BatSound.exe
Icon=/home/<USER>/.wine/drive_c/Program Files (x86)/Pettersson/Batsound4/img/batsound.png
Terminal=false
```
(Do not forget to change `<USER>` to your username.)

To have the icon, you can download [`batsound.png`](images/batsound.png).

### Configure Batsound to analyse ultrasounds
* Enter the `Sound/Sound Format` menu.

![Entering `Sound/Sound Forma` menu](images/batsound/enter-sound-format-menu.png)

* Change `Time expansion` value to 10 (according to your recorder settings)
* Keep 44 100 as `Samples per second` value.

* Enter the `Analysis/Spectrogram Settings - Defaults` menu.

![Entering `Spectrogram Setting - Defaults` menu](images/batsound/spectrogram-settings-default-values.png)


* Change `Max frequency` to `150000`;
* Set `Amplitude color mapping` to `Yellow, Red & Blue`;
* Adapt `Threshold` to sound intensity.

* You can zoom in.

With a *Pipistrellus kuhlii* record, I obtain the following spectrogram with the above settings : 
![pipkuh spectro](images/batsound/pipkuh-spectro-batsound.png)

Install 7-zip
-------------

7-zip is useful in Vigie-Chiro process to compress audio files for faster upload to [https://vigiechiro.herokuapp.com](https://vigiechiro.herokuapp.com).

1. Download `.exe` installer at [https://www.7-zip.org/](https://www.7-zip.org/)
2. Execute `7z1900.exe` with wine.

Install Lupas-Rename
--------------------

Lupas-Rename is used in Vigie-Chiro protocole to batch rename audio file to add protocoles informations such as pass and square.

1. Dowload `.exe` installer at [https://rename.lupasfreeware.org/download.php](https://rename.lupasfreeware.org/download.php)
2. Execute installer with wine
3. Batch rename audio files...

Conclusion
----------

With all these functionnal softwares, you are able to perform Vigie-Chiro protocole and sound analysis on GNU/Linux. I look forward to see your participation at [https://vigiechiro.herokuapp.com](https://vigiechiro.herokuapp.com) !