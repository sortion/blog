Title: How to mount a shared folder between Linux KVM Host and Guests
Date: 2022-06-21 11:47
Modified: 2022-06-21 11:47
Category: linux
Tags: qemu, kvm, linux
Slug: kvm-shared-folder
Authors: Samuel ORTION
lang: en

Sharing folder between KVM virtual machines and host, may be useful. Here is a way found in fedora forum.

## Quickstart

Change `vm` to your vm hostname.

```bash
sudo mkdir -p /mnt/shared
sudo chmod -R a+rwX /mnt/shared
sudo semanage fcontext -a -t svirt_home_t "/mnt/shared(/.*)?"
sudo restorecon -R /mnt/shared
tee fs.xml << EOF > /dev/null
<filesystem type='mount' accessmode='mapped'>
<source dir='/mnt/shared'/>
<target dir='shared'/>
</filesystem>
EOF
virsh shutdown vm
virsh attach-device vm fs.xml --config
virsh start vm
ssh vm
sudo mkdir -p /mnt/shared
sudo tee -a /etc/fstab << EOF > /dev/null
shared /mnt/shared 9p trans=virtio 0 0
EOF
sudo mount -a
```

## References

- [Virt-manager and shared folder host/guest permission issue?](https://ask.fedoraproject.org/t/virt-manager-and-shared-folder-host-guest-permission-issue/10938/5)