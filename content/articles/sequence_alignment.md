Title: Algorithmes d'alignement de séquences
Date: 2022-06-19 11:06
Modified: 2022-06-19 11:06
Category: bioinformatics
Tags: alignement, python
Slug: algorithmes-alignement-sequences
Authors: Samuel ORTION
Summary: 
lang: fr
Status: draft

En bioinformatique, l'alignement de séquences est souvent utilisé afin de déterminer l'homologie de deux séquences (ADN, ARN ou protéines). Il intervient par exemple dans l'outil BLAST, utilisé pour rechercher des séquences homologues à une séquence d'intérêt dans les grandes bases de données de gènes ou de protéines.


On distingues deux grands types d'algorithmes d'alignement de séquences: les alignements globaux (type algorithme de Needleman Wunsch) et les algorithmes d'alignement locaux (type algorithme de Smith et Waterman) . 

Ces deux algorithmes utilisent le même principe de programmation dynamique: on parcourent les deux séquence en rempliçant une matrice avec autant de colonnes et de lignes que la longueur de chaque séquence, en ajoutant un score en fonction des cases précédentes et les choix possibles (match, insertion, délétion). 

