Title: How to Classify Blood Cells Using Neural Networks
Date: 2022-06-18 07:44
Modified: 2022-06-18 07:44
Category: machine learning
Tags: biology
Slug: classify-blood-cells-using-neural-networks
Authors: Samuel ORTION
Summary: Machine Learning is widely used for image recognition. Here, we tried to perform blood cell classification using Convolutional Neural Networks.
lang: en
Status: draft

## Introduction

Machine Learning is widely used for image recognition. Here, we tried to perform blood cell classification using Convolutional Neural Networks.

There is a dataset of blood cells that can be used for training and testing [[2, 3]](#ref2).

According to the paper [[1]](#ref1), the model that performed the best is the Regionnal Convolutional Neural Network (R-CNN).

We tried to reproduce their results.

## The dataset

The dataset is a set of images of white blood cells and platelets sorted in 8 classes: immature granulocytes (ig), monocytes, basophils, neutrophils, eosinophils, erythroblasts and platelets.

The dataset contains 17092 images.


## The model

Firstly, we tried to use a classic Convolutional Neural Network (CNN).



## References
<span id="references"></span>

[1] [White blood cells detection and classification based on regional convolutional neural networks](https://www.sciencedirect.com/science/article/abs/pii/S0306987719310680?via%3Dihub) <span id="ref1"></span>

[2] [A dataset of microscopic peripheral blood cell images for development of automatic recognition systems](https://www.sciencedirect.com/science/article/pii/S2352340920303681?via%3Dihub) <span id="ref2"></span>

[3] [The precedent dataset, download page](https://data.mendeley.com/datasets/snkd93bnjr/1)
