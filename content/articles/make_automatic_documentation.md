Title: How to make automatic documentation using Doxygen, breathe and Sphinx
Date: 2022-06-15 11:56
Modified: 2022-06-15 12:35
Category: documentation
Tags: C++, Doxygen, Sphinx
Slug: automatized-code-documentation
Authors: Samuel ORTION
Summary: Doing documentation is required, to allow people to use your project. Here I present a rather easy solution.
lang: en


## Introduction

I recently wrote a C++ Library, and I wanted to document how to use it. I searched for a solution to extract the documentation from the code, and I found [Doxygen](https://www.doxygen.org/). It works well, but produces an ugly html output.

So I decided, with advices from the [JeBif](https://jebif.fr/) discord, to use Sphinx, to render the documentation.

Sphinx does not extract documentation from source code, it rather generates the documentation from Markdown or ReStructuredText files, so I still use Doxygen to do this job, and thanks to breathe, we can use its xml output to render documentation using Sphinx.

## Let's go !

### Setting all the stuff up

First of all, Install Doxygen.
```bash
apt-get install doxygen
```

Let us create a dummy example:

```bash
mkdir myawesomelibrary
cd myawesomelibrary
mkdir include
cd include
```

And create a dummy header file:

```c++
// myawesomelibrary/include/cat.hpp

/**
 * @brief This is a cat
 */
class Cat {
public:
    Cat() {
        say("I'm a cat");
    }
    /**
     * @brief the cat is saying meow 
     */
    void meow()
    {
        say("meow");
    }

    /**
     * @brief the cat is saying something 
     */
    void say(const std::string& message)
    {
        std::cout << message << std::endl;
    }
};
```

Let's generate the doxygen cofiguration file:

```bash
cd ..
doxygen -g Doxyfile
```

Then in this file, we have to set the path to the header files source directory, and allow doxygen to look up to source code files recursively. It is also time to set the output directory.

```text
INPUT = "./include"

EXTRACT_ALL = YES

RECURSIVE = YES

OUTPUT_DIRECTORY  =  "./doc/"
```

We need to tell doxygen to generate the xml output.

```text
GENERATE_XML = YES
```

And, we can disable html and $\LaTeX$ output.

```tex
GENERATE_HTML = NO

GENERATE_LATEX = NO
```

Now let's set up Sphinx

```bash
mkdir doc
cd doc
sudo apt install python-sphinx
sphinx-quickstart
```

In the `conf.py` file, we need to add the following lines:
```python
extensions = ['breathe']
breathe_projects = {'myawesomelibrary': '../xml'}
breathe_default_project = 'myawesomelibrary'
```

Of course we also need to install the `breathe` package.

```bash
pip install breathe
```

We need to tell Sphinx to render the class documentation:

```rst
// in `index.rst`
.. doxygenclass:: Cat
    :members:
```

We can use the theme from ReadTheDocs:

```bash
pip install sphinx_rtd_theme
```
```python
# in conf.py
html_theme = 'sphinx_rtd_theme'
```

### Generating the documentation

```bash
cd .. # go back to the root of the project
doxygen Doxyfile
cd doc
make html
```

That's it !

The output is in the `doc/build/html` directory.

Here is the result I got:

![sphinx dummy documentation](/images/sphinx/make_documentation_sphinx.png)