Title: Faire tourner Stable Diffusion sur Google Colab
Date: 2022-10-01 08:54
Modified: 2022-10-01 08:54
Category: art
Tags: ia, machine learning, python, notebook
Authors: Samuel ORTION
slug: run-stable-diffusion-on-colab
Status: published

## Introduction

Stable Diffusion est un modèle de deep learning permettant de générer des images photoréalistes à partir d'un *prompt* texte

J'ai récemment découver ce modèle via [lexica.art](https://lexica.art/), après avoir entendu parler des concurents DALL-E, Imagen et consorts.

Stable Diffusion a l'avantage d'être open source: tout le monde peut l'utiliser, et il fonctionne bien de surcroît.

## Prérequis

J'utilise un compte Google dédié, avec [Google Colab](https://colab.research.google.com/) pour faire tourner Stable Diffusion.

Le modèle devrait pouvoir tourner sur n'importe quelle plateforme Python, pourvu qu'il y ai assez de ressources GPU.

## Comment lancer Stable Diffusion

Sur Colab, importez le notebook [Art_Stable_Diffusion](/upload/stablediff/Art_Stable_Diffusion.ipynb) et lancer le.

Il faudra sans doute modifier le prompt dans le formulaire prévu à cet effet.

Ensuite vous pouvez générer les images et si les résultats vous plaisent, les télécharger ou les téléverser directement sur Drive.

Vous pouvez aussi utiliser le notebook original de Stable Diffusion comme expliqué dans l'[article Geekculture](https://medium.com/geekculture/2022-how-to-run-stable-diffusion-on-google-colab-5dc10804a2d7) en référence.

J'ai juste supprimé des bouts de codes qui ne me servaient pas et ajouté une cellule d'export sur Google Drive, avec édition des métadonnées (auteur, licence et surtout prompt utilisé).

## Résultats

Voici quelques images que j'ai obtenu avec Stable Diffusion:

<img src="/images/stablediff/fox_monet.png" alt="StableDiffusion's prompt: 'a painting of a fox sitting in a field at sunrise in the style of Claude Monet'" title="StableDiffusion's prompt: 'a painting of a fox sitting in a field at sunrise in the style of Claude Monet'">
<img src="/images/stablediff/horse.png" alt="StableDiffusion's prompt: 'A horse riding an astronaut'">

## References

- [Article Tuto Medium Geekculture (en)](https://medium.com/geekculture/2022-how-to-run-stable-diffusion-on-google-colab-5dc10804a2d7)