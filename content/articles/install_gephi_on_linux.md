Title: Install Gephi on Linux
Date: 2022-06-19 14:27
Modified: 2022-06-19 14:27
Category: linux
Tags: graph, visualization, linux
Slug: install-gephi-on-linux
Authors: Samuel ORTION
lang: en
Status: published

Gephi is a software package for graph visualization. Let's install it on Linux.

## Install Gephi

```bash
su - # Switch to root
cd /opt/
wget https://github.com/gephi/gephi/releases/download/v0.9.5/gephi-0.9.5-linux-x64.tar.gz -o
tar -xzf gephi-0.9.5-linux-x64.tar.gz
rm gephi-0.9.5-linux-x64.tar.gz
cd gephi-0.9.5
```

Now you could use it by running `./bin/gephi`  in the terminal.

## Setup desktop entry

For an easier access, you could add a desktop entry to your menu.

```text
// /home/$USER/.local/share/applications/gephi.desktop
[Desktop Entry]
Name=Gephi
Comment=Launch Gephi
Path=/opt/gephi-0.9.5/
Exec=/opt/gephi-0.9.5/bin/gephi
Terminal=true
Type=Application
Icon=gephi.jpg
StartupNotify=true
Categories=Development;Education;
Keywords=graph;
```

And download a logo for your application.

```bash
cd /opt/gephi-0.9.5/
wget https://dighumlab.org/wp-content/uploads/2020/03/gephi-tool-icon-200x200-1.jpg -o gephi.jpg
```

That's it !

## References

- [Gephi](https://gephi.org/)
