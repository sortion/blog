Title: Astuce: Copier du texte dans le presse-papier depuis un terminal Linux
Date: 2023-02-05 09:30
Modified: 2023-02-05 09:30
Category: tip
Tags: linux
Slug: xclip-clipboard-depuis-terminal
Authors: Samuel ORTION
Status: published
<!-- lang: fr -->

Il suffit d'installer `xclip`:

```bash
sudo dnf install xclip # sur fedora par exemple
```

Puis, c'est tout simple:

```bash
echo "Coucou !" | xclip -selection c
```

Un exemple d'utilisation: copier une clé publique ssh dans le presse papier depuis le terminal:

```bash
cat ~/.ssh/id_ecdsa.pub | xclip -selection c
```