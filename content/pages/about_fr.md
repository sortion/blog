Title: About
Date: 2021-03-24 15:40
Modified: 2021-03-25 14:00
Category: me
Tags: about, me, blog
Slug: about
Authors: Samuel ORTION
Summary: À propos.
lang:fr

Je suis Samuel ORTION, un *Geekus biologus* juvénile.

J'apprécie l'informatique, le machine learning, l'observation des oiseaux, la photographie, etc.

Je suis actuellement étudiant en licence double sciences de la vie - informatique à l'Université d'Évry val d'Essonne.

