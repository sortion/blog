Title: About
Date: 2021-03-24 15:40
Modified: 2021-03-25 14:00
Category: me
Tags: about, me, blog
Slug: about
Authors: Samuel ORTION
Summary: About me.
lang: en

I am Samuel ORTION, a juvenile *Geekus biologicus*.

I enjoy computer science, machine learning, bird watching, photography, etc.

I am currently a student at Université d'Évry val d'Essonne in *licence double Sciences de la Vie - Informatique*.
