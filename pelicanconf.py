#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import datetime

AUTHOR = 'Samuel ORTION'
SITENAME = 'BioloGeek\'s blog'
SITESUBTITLE = 'The Blog of Samuel ORTION, a juvenile <i>Geekus biologicus</i>'
SITEURL = 'https://blog.samuel.ortion.fr'
SITEDESCRIPTION = 'This is the Blog of Samuel ORTION, a juvenile <i>Geekus biologicus</i> enjoying computer science, machine learning, bird watching, taking photographs and more ;-).'
FAVICON = "images/favicon.ico"

DISABLE_URL_HASH = True

# PLUGIN_PATHS = ['plugins']
# PLUGINS = ['i18n_subsites', 'render_math']

I18N_SUBSITES = {
    'fr': {
        'SITENAME': 'Blog de Samuel ORTION',
        'SITESUBTITLE': 'Le blog de Samuel ORTION, un <i>Geekus biologicus</i> juvénile',
        'SITEDESCRIPTION': 'Ceci est le blog de Samuel ORTION, un jeune Geekus biologicus. Étudiant en bioinformatique, appréciant la biologie et l\'informatique entre autres choses',
        'LINKS': (('site web', 'https://samuel.ortion.fr'),
                  ('forge', 'https://forge.chapril.org/sortion'),
                  ('galerie', 'https://gallery.samuel.ortion.fr'))

    }
}

LANGUAGES = [
    ('en', '/', 'English'),
    ('fr', '/fr', 'Français'),
]
DEFAULT_LANG = 'en'

PATH = 'content'
STATIC_PATHS = ['images', 'upload']
TIMEZONE = 'Europe/Paris'

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

FAVICON = "favicon.ico"
MAIN_MENU = True
CC_LICENSE = {
    'slug': 'by-sa',
    'version': '4.0',
    'name': 'Attribution-ShareAlike'
}
CC_URL = "https://creativecommons.org/licenses/by-sa/4.0/"
COPYRIGHT_NAME = "Samuel ORTION"
COPYRIGHT_YEAR = datetime.datetime.now().year

GITEA_CORNER_URL = "https://forge.chapril.org/sortion/blog"

# Blogroll
LINKS = (('website', 'https://samuel.ortion.fr'),
        ('forge', 'https://forge.chapril.org/sortion'),
        ('gallery', 'https://gallery.samuel.ortion.fr'))

# Social widget
SOCIAL = (('pleroma', 'https://pleroma.s1gm4.eu/@samulus'),
    # ('mastodon', 'https://mamot.fr/@unclesamulus'),
            # ('twitter', 'https://twitter.com/SAmULuUus'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True 

THEME = 'themes/Flex'