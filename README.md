# [blog.samuel.ortion.fr](https://blog.samuel.ortion.fr/)

A blog from a Juvenile _Geekus biologicus_, by Samuel ORTION.

Repository for the blog source code and content, built with Pelican.

## Setup

### Clone

```bash
git clone --recurse-submodules ssh://gitea@forge.chapril.org:222/sortion/blog.git
```

### Install Python dependencies

_nb._ You should use a virtual environment.

```bash
pip install -r requirements.txt
```

### Render

```bash
make html
```

### Sync to remote

```bash
./sync.sh
```

Here it is, the [blog](https://blog.samuel.ortion.fr/) is up to date!
